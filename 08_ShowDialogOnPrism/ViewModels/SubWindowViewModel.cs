﻿using Prism.Commands;
using Prism.Ioc;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ShowDialogOnPrism.Views;

namespace ShowDialogOnPrism.ViewModels
{
    public class SubWindowViewModel : BindableBase
    {
        IContainerExtension ce;
        IRegionManager rm;

        public ICommand SelectedViewCommand { get; }

        public SubWindowViewModel(IContainerExtension _ce, IRegionManager _rm)
        {
            ce = _ce;
            rm = _rm;

            // 커맨드 등록
            SelectedViewCommand = new DelegateCommand<string>(View);

            // 뷰 등록
            ce.RegisterForNavigation<ViewA>();
            ce.RegisterForNavigation<ViewB>();

            // 첫 화면 초기화
            View(nameof(ViewA));
        }

        public void View(string ViewName)
        {
            rm.RequestNavigate(RegionNames.SubWindowContentView, ViewName);
        }
    }
}
