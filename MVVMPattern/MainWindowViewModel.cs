﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MVVMPattern
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        Human human;

        public string Name
        {
            get { return human.Name; }
            set { human.Name = value; OnPropertyChanged(); }
        }

        public int Age
        {
            get { return human.Age; }
            set { human.Age = value; OnPropertyChanged(); }
        }

        public ICommand Click { get; }

        public MainWindowViewModel()
        {
            human = new Human() { Name = "test", Age = 19 };

            Click = new RelayCommand(UpdateText);
        }

        void UpdateText()
        {
            Name = "hanje";
            Age = 29;
        }

        void test()
        {
            Name = "tet";
        }

        #region InotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
